import React, { Component } from 'react';
import { updatePosts, deletePosts, fetchMedia } from '../common/utils';
import './post.css';

class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEditing: false,
            id: null,
            title: null,
            image: null,
        }

        this.changeTitle = this.changeTitle.bind(this);
        this.editPost = this.editPost.bind(this);
        this.updatePost = this.updatePost.bind(this);
        this.deletePost = this.deletePost.bind(this);
    }

    changeTitle(event) {
        this.setState({ title: event.target.value });
    }

    editPost() {
        this.setState(prevState => ({
          isEditing: !prevState.isEditing,
          title: this.props.title,
        }));
    }

    updatePost() {
        this.setState(prevState => ({
          isEditing: !prevState.isEditing
        }));
        console.log(this.state.title);
        updatePosts(this.state.id, this.state.title);
    }

    deletePost() {
        deletePosts(this.props.id)
            .then(this.props.reloadApp);
    }

    render() {
        const { isEditing } = this.state;
        return (
            <div>
                <li className="post">
                    <img src={this.state.image} alt=""/>
                    <div className="post-titlebox">
                        {!isEditing ?
                            <div>{this.state.title}</div> :
                            <input className="post-editbox" type="text" value={this.state.title} onChange={this.changeTitle}/>
                        }
                    </div>
                    <div className="post-buttonbox">
                        {!isEditing ?
                            <button className="button" onClick={this.editPost}>Edit</button> :
                            <span>
                            <button className="button" onClick={this.editPost}>Cancel</button><button className="button" onClick={this.updatePost}>Save</button>
                            </span>
                        }
                    <button className="button" onClick={this.deletePost}>Delete</button>
                    </div>
                </li>
            </div>
        )
    }
    componentDidMount() {
        this.setState({ title: this.props.title });
        this.setState({ id: this.props.id });
        fetchMedia(this.props.image)
            .then(image => this.setState({ image: image.media_details.sizes.medium.source_url }))
            .catch(error => console.log(error));
    }
}

export default Post;
import React, { Component } from 'react';
import './App.css';
import { fetchPosts } from '../common/utils'
import Loading from '../common/loading';
import Post from '../post/post';


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isLoading: true,
        }

        this.setPosts = this.setPosts.bind(this);
        this.reloadApp = this.reloadApp.bind(this);
    }

    reloadApp() {
        fetchPosts()
            .then(posts => this.setPosts(posts));
    }

    setPosts(posts) {
        console.log('setPosts')
        this.setState({ posts: [] })
        this.setState({ posts: posts, isLoading: false });
    }

    render() {
        const { isLoading, posts } = this.state;
        if(isLoading) {
            return(
                <Loading/>
            )
        } else {
            return (
                <div className="App">
                    <ul>
                        {posts.map(post => <Post title={post.title.rendered} id={post.id} image={post.featured_media} reloadApp={this.reloadApp} />)}
                    </ul>
                </div>
            );
        }
    }

    componentDidMount() {
      fetchPosts()
          .then(posts => this.setPosts(posts))
          .catch(error => console.log(error))
    }
}

export default App;

import React, { Component } from 'react';

class Loading extends Component {
    render() {
        return (
            <div className="loading">
                Loading posts...
            </div>
        )
    }
}

export default Loading;
function fetchPosts() {
    return fetch(`/wp-json/wp/v2/posts/?per_page=5&order=desc`)
        .then(response => response.json())
        .catch(error => console.log(error));
}

function updatePosts(id, title) {
    return fetch(`/wp-json/wp/v2/posts/${id}?title=${title}`, {
        method: "POST",
        headers: {
            "Authorization": "Basic " + btoa("romafederico:i3ni6NCwab"),
        },
    })
}

function deletePosts(id) {
    return fetch(`/wp-json/wp/v2/posts/${id}`, {
        method: 'DELETE',
        headers: {
            "Authorization": "Basic " + btoa("romafederico:i3ni6NCwab"),
        },
    })
}

function fetchMedia(id) {
    return fetch(`/wp-json/wp/v2/media/${id}`)
        .then(response => response.json())
        .catch(error => console.log(error));
}

export {
    fetchPosts,
    updatePosts,
    deletePosts,
    fetchMedia,
}